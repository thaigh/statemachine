﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StateMachine
{
    /// <summary>
    /// Defines the mappings between state machine states and the implementation MachineStates
    /// </summary>
    /// <typeparam name="T">The key value for the Machine State. Generally an Enum type</typeparam>
    public class StateMap<T> : Dictionary<T, MachineState<T>>
    {
    }
}
